# Length was the problem! ;-)

I need to resize the PV so the remaining 'free" extents are divisible by 32!

# Precondition
Original extents = 4m

I've moved my lv_root to the very start of the PV!


# Problem
`pvs --segments -o+lv_name,seg_start_pe,segtype`

Looking at:  
  PV         VG  Fmt  Attr PSize   PFree  Start SSize LV      Start Type  
  /dev/sda2  vg0 lvm2 a--  <93.75g 90.62g     0   800 lv_root     0 linear  
  /dev/sda2  vg0 lvm2 a--  <93.75g 90.62g   800 23199             0 free  
  /dev/sdb   add lvm2 a--  <99.97g 96.84g     0   800 new         0 linear  
  /dev/sdb   add lvm2 a--  <99.97g 96.84g   800 24792             0 free  


## Math
24792*4 % 32 = 0	# in vg0

But  
23199*4 % = 28!!	# in add


# Solution
Makes PV just large enough to change extentsize!

`pvresize --setphysicalvolumesize 96004m /dev/sda2`

## Test
Now this works:

`vgchange --physicalextentsize 32m vg0`
`vgchange --physicalextentsize 64m vg0`
`vgchange --physicalextentsize 128m vg0`

;-)
