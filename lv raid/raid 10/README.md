# Performance!
https://en.wikipedia.org/wiki/Non-standard_RAID_levels#Linux_MD_RAID_10

'''
The four-drive example is identical to a standard RAID 1+0 array, while the three-drive example is a software implementation of RAID 1E. The two-drive example is equivalent to RAID 1.[12]

The driver also supports a "far" layout, in which all the drives are divided into f sections. All the chunks are repeated in each section but are switched in groups (for example, in pairs). For example, f2 layouts on two-, three-, and four-drive arrays would look like this:[11][12]
'''

## Far layouts
https://raid.wiki.kernel.org/index.php/A_guide_to_mdadm#Near.2C_Far.2C_and_offset_layouts