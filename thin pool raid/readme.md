sch: https://www.google.com/search?q=lvm+thin+on+raid

# Guide:
https://www.systutorials.com/docs/linux/man/7-lvmthin/#lbAO

https://myles.sh/migrating-a-lvm-thinpool-to-a-raid-environment/

# Doc
[1.4.4. Thinly-provisioned logical volumes (thin volumes)](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_logical_volumes/logical_volumes-configuring-and-managing-logical-volumes#con_thin-volumes_lvm-logical-volumes)

[1.4.6. Thinly-provisioned snapshot volumes](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_logical_volumes/logical_volumes-configuring-and-managing-logical-volumes#con_thin-snapshot-volumes_lvm-logical-volumes)

[Thin RAID5 logical volumes: Gentoo](https://wiki.gentoo.org/wiki/LVM#Thin_RAID5_logical_volumes)