# Checking data coherency
# https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_logical_volumes/assembly_configure-mange-raid-configuring-and-managing-logical-volumes#proc-scrubbing-raid-volume-configure-manage-raid


specificLV(){
lvs -o +raid_sync_action,raid_mismatch_count vg/lv
}

Works(){
lvs -o +raid_sync_action,raid_mismatch_count vg0
}