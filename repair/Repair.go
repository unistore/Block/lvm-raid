# Repair detached or failed disks!
# prime. access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_logical_volumes/assembly_configure-mange-raid-configuring-and-managing-logical-volumes#proc-scrubbing-raid-volume-configure-manage-raid

$vg=
$raid_lv=

Repair(){
lvchange --syncaction repair $vg/$raid_lv
}

Monitor(){
lvs -o +raid_sync_action,raid_mismatch_count $vg/$raid_lv
}
