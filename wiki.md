# lvm-raid
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_logical_volumes/index

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_logical_volumes/assembly_configure-mange-raid-configuring-and-managing-logical-volumes

# Resync status
'''The raid_sync_action field displays the current synchronization operation that the raid volume is performing. It can be one of the following values:

    idle: All sync operations complete (doing nothing)
    resync: Initializing an array or recovering after a machine failure
    recover: Replacing a device in the array
    check: Looking for array inconsistencies
    repair: Looking for and repairing inconsistencies 
'''
[prime](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_logical_volumes/assembly_configure-mange-raid-configuring-and-managing-logical-volumes#proc-scrubbing-raid-volume-configure-manage-raid)

# --nosync
--nosync
              Causes the creation of mirror, raid1, raid4, raid5 and raid10 to
              skip the initial synchronization. In case of mirror, raid1 and
              raid10, any data written afterwards will be mirrored, but the
              original contents will not be copied. In case of raid4 and
              raid5, no parity blocks will be written, though any data written
              afterwards will cause parity blocks to be stored.  This is use‐
              ful for skipping a potentially long and resource intensive ini‐
              tial sync of an empty mirror/raid1/raid4/raid5 and raid10 LV.
              This option is not valid for raid6, because raid6 relies on
              proper parity (P and Q Syndromes) being created during initial
              synchronization in order to reconstruct proper user date in case
              of device failures.  raid0 and raid0_meta do not provide any
              data copies or parity support and thus do not support initial
              synchronization.
[prime](ex://man lvcreate)

# lvs Raid type
https://unix.stackexchange.com/questions/388518/how-to-tell-if-a-logical-volume-is-striped

