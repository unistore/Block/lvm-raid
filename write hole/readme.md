https://en.wikipedia.org/wiki/RAID#WRITE-HOLE

Recently mdadm fixed it by introducing a dedicated journaling device (to avoid performance penalty, typically, SSDs and NVMs are preferred) for that purpose.